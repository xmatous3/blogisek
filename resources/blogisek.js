var ttc = document.getElementById('tooltipcloser');
var dim = document.getElementById('dimmer').classList;
ttc.onclick = (ev) => {
    let ttip = ttc.ttip;
    ttip && ttip.classList.remove('open');
    ttc.hidden = true;
    dim.remove('on');
};
for ( e of document.getElementsByClassName('was') ) {
    e.dataset.tooltip = e.title;
    e.title = '';
    if ( e.tagName == 'A' )
        continue;
    e.onclick = (ev) => {
        let classes = ev.target.classList;
        let wasopen = classes.contains('open');
        if ( ttc.ttip ) { classes.remove('open'); }
        ttc.ttip = ev.target;
        classes.toggle('open', !wasopen);
        ttc.hidden = wasopen;
        dim.toggle('on', !wasopen);
    };
}
var tsel = document.getElementById('theme-selector');
if ( tsel ) {
    for ( e of tsel.getElementsByTagName('button') ) {
        e.onclick = (ev) => {
            let t = ev.target.dataset.theme;
            window.localStorage.theme = t;
            document.firstElementChild.dataset.theme = t;
        };
    }
    tsel.style = null;
}
