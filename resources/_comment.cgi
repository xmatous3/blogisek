#!/usr/bin/perl

# TODO Rewrite. This is a dumpster fire.

# Expects a comment in the POST parameters

use strict;
use warnings;
use utf8;
use open qw/:std :encoding(utf-8)/;

use CGI::Simple;
use File::Slurper qw/read_text read_lines/;
use POSIX 'strftime';
use File::Temp qw/tempfile/;
use Unicode::Normalize;

$CGI::Simple::POST_MAX = 8192; # 8 Kib
$CGI::Simple::DISABLE_UPLOADS = 1;
$CGI::Simple::PARAM_UTF8 = 1;
#$CGI::Simple::DEBUG = 1;

my $q = CGI::Simple->new;

my @header = qw/-charset utf-8/;
my $dir = "_comments";
my $suffix = ".comment";
my $queue_limit = 50;

sub errorpage {
    print $q->header( @header, -status => $_[0] );
    print_errorpage( $_[0] );
    exit 0;
    # dbg
    use Data::Dumper;
    print Dumper @_;
}

unless ( -t STDIN || ( $ENV{REQUEST_METHOD} // '' ) eq 'POST' )
{
    exit errorpage( '405 Method Not Allowed' );
}

# When no 'article' parameter has been given, try to extract the
# article id (slug) from the URI
my $article_slug = $ENV{REQUEST_URI} =~ s,/comment(\?.*)?$,,rn;
$article_slug =~ s,^(https?://[^/]*)?/,,n;
my %c = (
    article  => fixws( scalar $q->param( 'article' ) // $article_slug ),
    author   => fixws( scalar $q->param( 'author' ) ),
    body     => fixws( scalar $q->param( 'body' ) ),
    password => fixws( scalar $q->param( 'recog' ) // '' ),
    parent   => fixws( scalar $q->param( 'replyto' ) // '' ),
);

use Data::Dumper;
print STDERR Dumper(\%c);

unless ( $c{article} && $c{article} =~ m,^[-/a-z0-9]+$,
      && $c{author} && 0 < length $c{author} < 64 && no_weird_chars( $c{author} )
      && $c{body} && 0 < length $c{body} < 8000 && no_weird_chars( $c{body} )
      && ( !$c{parent} || $c{parent} =~ /^[0-9]{14}\w{4}$/ ) ) # TODO: other ID formats
{
    exit errorpage( '400 Bad Request' );
}

my $comment_index = load_index( '_comment_index' );
my $password_index = load_index( '_comment_passwords', 1 );

unless ( $password_index->{$c{article}}     # comments are enabled
      && ( !$c{parent} || $comment_index->{$c{article}}{$c{parent}} ) ) # parent exists
{
    exit errorpage( '400 Bad Request' );
}

sanitise( $c{author} );
sanitise( $c{password} );

my $password = password_normalise( $c{password} );
my $passed = $password && ( # index might contain empty password, if comments are enabled and no password is provided
                 $password_index->{$c{article}}{$password} # article password
              || $password_index->{'*'}{$password}         # global passwords
          );

# Deny if the queue is full, unless the password is correct
my @ls = glob( "$dir/*$suffix" );
if ( !$passed && @ls >= $queue_limit )
{
    exit errorpage( '503 Service unavailable' );
}

$c{parent} ||= 'root'; # top-level comments

$c{body} =~ s/\v/\n/sg;
$c{body} =~ s/^\n*//s;
$c{body} =~ s/\n*$//s;

my @now = localtime;
my $date = strftime "%F %T %Z", @now;
my $slug = strftime "%Y%m%d%H%M%S", @now;

my ($fh, $filename) = tempfile( "${slug}XXXX", DIR => $dir, SUFFIX => $suffix );
my $commentid = $filename =~ s/^\Q$dir\E\/(.+)\Q$suffix\E$/$1/r;

unless ( -t STDIN ) {
    my $gid = getgrnam "adamat";
    chown -1, $gid, $filename;
    chmod 0440, $filename;
}

my $comment = <<EOF;
Article: $c{article}
Author:  $c{author}
Date:    $date
Pass:    @{[ $passed ? 'yes' : 'no' ]}\t$c{password}
Stamp:   @{[time]}
Id:      $commentid
Parent:  $c{parent}

$c{body}
EOF

binmode $fh, ':encoding(utf-8)';
print $fh $comment;
close $fh;

print $q->header(
    -status => '303 See Other',
    -location => $passed ? '/comment-accepted.html' : '/comment-queued.html',
);
exit 0;


sub print_errorpage {
    my $status = shift;
    my ($code) = $status =~ /^(\d+)/;
    my $file = "/var/www/errorpages/$code.html";
    if ( -r $file ) {
        print read_text( $file );
    }
    else {
        print "Status $status\n";
    }
}

sub fixws {
    $_ = shift;
    s/\r\n/\n/gs;
    s/\t/ /g;
    return $_;
}

sub no_weird_chars {
    $_ = shift;
    return /^[\P{Control}\n]*$/s && !/\p{Surrogate}]/s;
}

sub sanitise {
    for ( shift ) {
        s/\s+/ /gs;
        s/^ +//;
        s/ +$//;
    }
}

sub password_normalise {
    $_ = lc NFKD( shift ); # unicode normalisation, lowerspace
    s/[^-a-z0-9 ]//g;
    return $_;
}

sub load_index {
    my @content = read_lines( shift );
    my $normalise = shift;
    my %index;
    foreach ( @content ) {
        my @fields = split / *\t */;
        next unless @fields;
        # comment ids aren't any wierder than normalised passwords, anyway
        my $d = @fields == 1 ? '#' # unattainable password to indicate that comments are enabled
              : $normalise ? password_normalise( $fields[1] ) : $fields[1];
        $index{$fields[0]}{$d} = 1;
    }
    return \%index;
}

# vim: sts=4 et ts=8 sw=4
