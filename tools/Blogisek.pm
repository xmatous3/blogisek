#!/usr/bin/perl
package Blogisek;
use Exporter 'import';
our @EXPORT = qw/template $template $srcdir $outdir $root load_comment load_meta load_index/;
our @EXPORT_OK = qw/load_822/;

use strict;
use warnings;
use utf8;
use open qw/:std :encoding(utf-8)/;
use experimental 'signatures';

use Template;
$Template::BINMODE = ':encoding(utf-8)';
use Time::Piece;
use Date::Parse;
use File::Slurper qw/read_lines/;
use Encode qw/decode_utf8/;
use Guard;
use POSIX qw/locale_h/;

# Report use of this module to gib
open my $gibfh, '+<&3';
print $gibfh "dep tools/Blogisek.pm\n";
print $gibfh "dep templates/defs\n";
close $gibfh;

our ($srcdir, $outdir) = @ENV{qw/SRCDIR OUTDIR/};
$srcdir //= '.';
$outdir //= '.';

our @indexfields = qw/id title category ncomments stamp banner banner_alt post_format title_plain/;

our $root = $ENV{DOCUMENT_ROOT} // '';
my $root_absolute = 'https://blogisek.adamat.dev';

my $now = localtime;
our $template = Template->new({
        ENCODING     => 'utf8',
        PRE_CHOMP    => 0,
        POST_CHOMP   => 0,
        TRIM         => 0,
        INCLUDE_PATH => ".:$srcdir/templates:$srcdir",
        RECURSION    => 1,              # for comment replies
        FILTERS      => {
            utf8 => sub { decode_utf8( $_[0] ); },
            rsshacks => \&rss_filter,
        },
        PRE_PROCESS  => 'defs',
        # COMPILE_DIR  => '_tt',
        # COMPILE_EXT  => '.pl',
        VARIABLES    => {
            root => $root,
            root_absolute => $root_absolute,
            now => $now,
            datef => sub { $_[0]->ymd },
            timef => sub { $_[0]->strftime( '%H.%M' ) },
            isostamp => sub { $_[0]->strftime( '%FT%T%z' ) },
            rfc822stamp => \&rfc822stamp,
            plur => \&plural,
            qotd => \&qotd,
            genimgattr => \&genimgattr,
        },
        # TODO: pre-compilation?
    });

sub template { $template };

sub load_comment($file) {
    my ($h, $b) = load_822( $file );
    $h->{id} ||= $file =~ s,^(?:.*/)(.*).comment$,$1,r;
    chomp $b;
    return {
        %$h,
        body => $b,
    };
}

sub load_meta($src) {
    my ($file, $article) = ($src, $src);
    $article =~ s,/meta$,,;
    $article =~ s,^(\Q$srcdir\E)?/?(articles/)?,,n;
    $file .= '/meta' if $src !~ m,/meta$,;
    unless ( $file =~ m,^/, ) {
        for ( '.', 'articles', "$srcdir/articles" ) {
            my $tryf = "$_/$file";
            next unless -r $tryf;
            $file = $tryf;
            last;
        }
    }

    my ($h, $b) = load_822( $file );

    $h->{password} = [ $h->{password} ]
        if exists $h->{password} && ref $h->{password} ne 'ARRAY';
    $h->{password} //= [];
    $h->{password} = [ map { /(.+?)\t(.+)/ ? { q => $1, a => $2 } : () } $h->{password}->@* ];
    $h->{id} ||= $article;
    $h->{path} ||= $file =~ s,/meta$,,r;
    my $cindexf = "$article/comment_index";
    my @comments = -r $cindexf ? read_lines( $cindexf ) : glob( "$h->{path}/*.comment" );
    $h->{ncomments} = 0+@comments;
    if ( defined $h->{comments} ) {
        $h->{comments_disabled} = !str_to_bool( $h->{comments} );
        delete $h->{comments};
    }

    # Hack connecting "micro-posts" with the "/micro" category (both ways)
    $h->{category} = 'micro' if $h->{title} =~ /^\p{Emoji}*$/;
    $h->{post_format} //= 'micro' if $h->{category} eq 'micro';
    $h->{category} //= 'micro' if $h->{post_format} eq 'micro';
    $h->{post_format} //= 'normal';
    if ( $h->{post_format} eq 'micro' ) {
        $h->{title_plain} = $h->{title};
        $h->{title} = "µ: $h->{title}";
    }

    $h->{category} =~ s,^/,, if $h->{category};
    $h->{category} ||= '.';
    if ( $h->{banner} ) {
        $h->{banner} =~ s/\t(.*)//;
        $h->{banner_alt} ||= $1;
    }
    $h->{banner} //= '';
    $h->{banner_alt} //= '';
    $h->{title_plain} //= $h->{title};

    return {
        %$h,
    };
}

# Almost general, but converts date (string) -> stamp (epoch) -> published (Time::Piece)
sub load_822($file) {
    open my $fh, '<', $file
        or die "load_meta: cannot open '$file': $!";
    my %headers;
    while ( <$fh> ) {
        last if /^$/;
        /^([^\s:]+):\s*(.*)$/
            or warn "Unable to parse line '$_'", next;
        my $hname = lc $1;
        my $hval = $2;
        $hname =~ s/-/_/g;
        if ( !defined $headers{$hname} ) {
            $headers{$hname} = $hval;
        }
        elsif ( ref $headers{$hname} eq 'ARRAY' ) {
            push $headers{$hname}->@*, $hval;
        }
        else {
            $headers{$hname} = [ $headers{$hname}, $hval ];
        }
    }
    my $body = join '', <$fh>;
    close $fh;

    $headers{stamp} = str2time( $headers{date} ) // 0
        if ( !exists $headers{stamp} );
    $headers{published} = localtime $headers{stamp};

    return \%headers, wantarray ? $body : ();
}

sub str_to_bool($str) {
    return 1 if $str =~ /^(on|enabled?|yes|1|true)$/ni;
    return 0 if $str =~ /^(off|disabled?|no|0|false)$/ni;
    die "Don't know how to convert '$str' to bool.";
}

sub load_index($file) {
    $file .= '/index' if -d $file;
    my @index;
    for my $l ( read_lines( $file ) ) {
        my %record;
        @record{@indexfields} = split "\t", $l;
        $record{published} = localtime $record{stamp};
        push @index, \%record;
    }
    return @index;
}

sub plural($n, $sg, $du, $pl) {
    $n %= 100;
    return $pl if 11 <= $n <= 14;
    $n %= 10;
    return $sg if $n == 1;
    return $du if 2 <= $n <= 4;
    return $pl;
}

my @quotes;
sub qotd() {
    unless ( @quotes ) {
        my $quotefile = "$srcdir/articles/quotes";
        @quotes = read_lines( $quotefile ) if -r $quotefile;
        push @quotes, 'Moudra došla.' unless @quotes;
    }
    my $i = int( rand @quotes );
    return $quotes[ $i ];
}

sub genimgattr($src, $mini=0) {
    my ($pref, $suff) = $src =~ m,^(.*?)(\.\w+)?$,;
    $pref = "$root/$pref";
    my $attrs = "src='$pref.380$suff'";
    my $srcset = join ', ', map "$pref.$_$suff ${_}w", qw/380 768/;
    my $sizes = $mini ? '380px' : '(max-width:380px) 380px, 768px';
    $attrs .= " srcset='$srcset' sizes='$sizes'";
    return $attrs;
}

sub rfc822stamp($t) {
    my $original = setlocale( LC_TIME );
    my $guard = scope_guard { setlocale( LC_TIME, $original ) };
    setlocale( LC_TIME, 'C' );
    return $t->strftime( '%a, %d %b %Y %T %z' );
}

sub rss_filter($s) {
    $s =~ s,(\b(href)\s*=\s*['"]?)/,$1$root_absolute/,gi;
    return $s;
}

1;
